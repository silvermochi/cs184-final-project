#include "Containers.h"
#include "Particles.h"

Containers::Containers(double x, double y, double z, double l, double w, double h) {
    Container con;
    con.p = glm::dvec3(x, y, z);
    con.d = glm::dvec3(l, w, h);
    container = con;
}

void Containers::render() {
    glPushMatrix();
    glTranslatef(container.p.x, container.p.y, container.p.z);
    glutSolidCube(1);
    glPopMatrix();
}
