#include "Particles.h"
#include "glm/glm.hpp"
#include <unordered_set>
#include <unordered_map>
#include <vector>
#if defined(__APPLE_CC__)
#include <GLUT/glut.h>
#else
#include <OpenGL/glut.h>
#include <math.h>
#endif

class Containers {
public:
    Containers(double x, double y, double z, double l, double w, double h);
    double tk = 0.01;
    void render();
private:
    struct Container {
        glm::dvec3 d;   // Dimension
        glm::dvec3 p;   // Center of base
        glm::dvec3 v = glm::dvec3(0.0, 0.0, 0.0);
        glm::dvec3 force = glm::dvec3(0.0, 0.0, 0.0);
    };
    Container container;
};
