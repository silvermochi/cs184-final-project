/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Particles.h
 * Author: swl
 *
 * Created on April 15, 2016, 12:16 PM
 */

#ifndef PARTICLES_H
#define PARTICLES_H

#include <../glm.hpp>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#if defined(__APPLE_CC__)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#define _USE_MATH_DEFINES
#include <math.h>
#endif

class Particles {
public:
    Particles();
    glm::dvec3 gravity = glm::dvec3(0, -9.8, 0);
    double mass = 1;
    double delta_t = 0.005;
    int nx = 30;
    int ny = 30;
    //int nz = 10;
    float d = 0.05;
    double h = 0.1;
    double p0 = 5000.0;
    double ep = 500.0;
    double c = 0.0001;
    void buildGrid();
    void render();
    void update();
    void updatePos(glm::dvec3 &pos, int &flag);
    double hash_position(glm::dvec3 pos);
    double calc_distance(glm::dvec3 po1s, glm::dvec3 pos2);
    void buildMap();
    double WPoly(double r, double h);
    double WSpiky(double r, double h);
    void step(){} // simulate one frame
    bool collide(glm::dvec3 p);
private:
    struct Particle
    {
        glm::dvec3 p;
        glm::dvec3 v;
        int flag = 0;
        glm::dvec3 predicted_p;
        glm::dvec3 predicted_v;
        glm::dvec3 force;
        std::vector<Particle *> *neighbors;
        double lambda_i;
        glm::dvec3 delta_p;
        glm::dvec3 vort;
        glm::dvec3 f_vort;
    };

    std::vector<Particle> particles;
    std::unordered_map<double, std::vector<Particle *> *> map;
    double spiky(Particle par1, Particle par2);
};

#endif /* PARTICLES_H */
