/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * File:   Particles.cpp
 * Author: swl
 *
 * Created on April 15, 2016, 12:16 PM
 */

//the container with x range (-1.0, 1.0), z range (-1.0, 1.0), y range (-2.0, ...)



#include "Particles.h"
#define _USE_MATH_DEFINES
#include <math.h>

Particles::Particles()
{
    for(int x=0; x<nx; x++)
    {
        for(int y=0; y<ny; y++)
        {
//            for(int z=0; z<nz; z++)
//            {
                Particle par;
//            double w = 1.0/ (nx - 1);
//            double h = 1.0/ (ny - 1);
//                par.p = glm::dvec3(-0.5 + x * w, -0.5 + y * h, 0);
            par.p = glm::dvec3((x+0.5-nx*0.5)*d, (y+0.5)*d, 0);
            par.v = glm::dvec3(0.0, 0.0, 0.0);
            par.neighbors = new std::vector<Particle *>();
            particles.push_back(par);
            //}
        }
    }
}

//void Particles::updatePos(glm::dvec3 &pos, int &flag)
//{
//    if (flag == 0) {
//        pos.y -= 0.05;
//        if (pos.y < -2.0) {
//            flag = 1;
//        }
//    } else {
//        pos.y += 0.05;
//        if (pos.y > 0) {
//            flag = 0;
//        }
//    }
//
//}


double Particles::spiky(Particle par1, Particle par2)
{
    glm::dvec3 sum = glm::dvec3(0.0, 0.0, 0.0);
    if (par1.predicted_p == par2.predicted_p) {
        for (Particle *neighbor: *par1.neighbors) {
            double distance = calc_distance(par1.predicted_p, neighbor->predicted_p);
            glm::dvec3 gradient = (par1.predicted_p - neighbor->predicted_p) * WSpiky(distance, h);
            //printf("%f\n",  WSpiky(distance, h));
            sum += gradient;
        }
    } else {
        double distance = calc_distance(par1.predicted_p, par2.predicted_p);
        glm::dvec3 gradient = -1.0 * (par1.predicted_p - par2.predicted_p) * WSpiky(distance, h);
        sum += gradient;
    }
    sum /= p0;
//    printf("%f\n", pow(sum.x, 2) + pow(sum.y, 2) + pow(sum.z, 2));
    return pow(sum.x, 2) + pow(sum.y, 2) + pow(sum.z, 2);
}

void Particles::update()
{
    for(Particle &par : particles)
    {
        par.force = mass * gravity;
        par.v += delta_t * par.force;
        par.predicted_p = par.p + delta_t * par.v;
        
    }
    buildMap();
    for(Particle &par : particles){
        (*par.neighbors).clear();
        glm::dvec3 start = glm::dvec3(-2.0*h, -2.0*h, -2.0*h);
        for (int i = 0; i < 3; i++) {
            start.x += h;
            start.y = -2.0*h;
            for (int j = 0; j < 3; j++) {
                start.y += h;
                start.z = -2.0*h;
                for (int k = 0; k < 3; k++) {
                    start.z += h;
                    glm::dvec3 pos = par.p + start;
                    double hashneighbor = hash_position(pos);
                    if (map.find(hashneighbor) != map.end()) {
                        for (Particle *par_neighbor: *map[hashneighbor]) {
                            if (calc_distance(par.predicted_p, par_neighbor->predicted_p) <= h
                                && calc_distance(par.predicted_p, par_neighbor->predicted_p) > 0) {
                                par.neighbors->push_back(par_neighbor);
                            }
                        }
                    }
                }
            }
        }
    }
//
//    for(Particle &par : particles) {
//        (*par.neighbors).clear();
//        for(Particle &neighbor : particles) {
//            double distance =calc_distance(par.predicted_p, neighbor.predicted_p);
//            if (distance <= h) {
//                (*par.neighbors).push_back(&neighbor);
//            }
//        }
//    }
    
    for (int i = 0; i < 10; i++){
        for (Particle &par : particles){
            //calculate λ_i
            double currDensity = 0.0;
            double currPkCi = 0.0;
            //double j = 1;
            for (Particle *neighbor : *par.neighbors){
                double distance = calc_distance(par.predicted_p, neighbor->predicted_p);
                //printf("%f\n", j);
                currDensity += mass * WPoly(distance, h);
                currPkCi += spiky(par, *neighbor);
                //j++;
            }
            par.lambda_i = -(currDensity/p0 - 1.0)/(currPkCi + ep);
        }
        for (Particle &par : particles){
            //calculate ∆p_i
            glm::dvec3 sum = glm::dvec3(0.0, 0.0, 0.0);
            for (Particle *neighbor : *par.neighbors){
                double distance =calc_distance(par.predicted_p, neighbor->predicted_p);
                sum += (neighbor->lambda_i + par.lambda_i)
                * (par.predicted_p - neighbor->predicted_p) * WSpiky(distance, h);
//                double s_corr = -0.0001 * pow(WPoly(distance, h)/WPoly(0.3 * h, h), 4);
//                sum += (neighbor->lambda_i + par.lambda_i + s_corr)
//                 * (par.predicted_p - neighbor->predicted_p) * WSpiky(distance, h);
				//double s_corr = -0.0001 * pow(WPoly(distance, h) / WPoly(0.3 * h, h), 4);
				//sum += (neighbor->lambda_i + par.lambda_i + s_corr)
				//	* (par.predicted_p - neighbor->predicted_p) * WSpiky(distance, h);
                
            }
            par.delta_p = sum * 1.0 / p0;
       }
        for (Particle &par : particles){
            par.predicted_p += par.delta_p;
            if (par.predicted_p.x < -1.0)
                par.predicted_p.x = -1.0;
            if (par.predicted_p.x > 1.0)
                par.predicted_p.x = 1.0;
            if (par.predicted_p.y < -1.5)
                par.predicted_p.y = -1.5;
            if (par.predicted_p.y > 2.0)
                par.predicted_p.y = 2.0;
            // if (par.predicted_p.z < -1.0)
            //     par.predicted_p.z = -1.0;
            // if (par.predicted_p.z > 1.0)
            //     par.predicted_p.z = 1.0;
        }
    }

    for (Particle &par : particles) {
        par.v = (par.predicted_p - par.p) / delta_t;
        par.vort = glm::dvec3(0.0, 0.0, 0.0);
        for (Particle *neighbor : *par.neighbors) {
            double distance =calc_distance(par.predicted_p, neighbor->predicted_p);
            par.vort += glm::cross(neighbor->v - par.v,
                        (par.predicted_p - neighbor->predicted_p)* WSpiky(distance, h));
        }
    }
    for (Particle &par : particles) {
        glm::dvec3 eta = glm::dvec3(0.0, 0.0, 0.0);
        for (Particle *neighbor : *par.neighbors) {
            double distance =calc_distance(par.predicted_p, neighbor->predicted_p);
            double w_abs = sqrt(pow(neighbor->vort.x, 2) + pow(neighbor->vort.y, 2)
                                  + pow(neighbor->vort.z, 2));
            eta += w_abs * (par.predicted_p - neighbor->predicted_p)* WSpiky(distance, h);
        }
        double eta_abs = sqrt(pow(eta.x, 2) + pow(eta.y, 2)
                              + pow(eta.z, 2));
        par.f_vort = 0.0001 * glm::cross(eta * 1.0 / eta_abs, par.vort);
        par.v += delta_t * par.f_vort;
    }
    for (Particle &par : particles) {
        par.predicted_v = par.v;
        for (Particle *neighbor : *par.neighbors) {
            double distance =calc_distance(par.predicted_p, neighbor->predicted_p);
            par.predicted_v += c * (neighbor->v - par.v) * WPoly(distance, h);
            //printf("%f\n", par.predicted_v.x);
        }
    }
    for (Particle &par : particles) {
        par.v = par.predicted_v;
        par.p = par.predicted_p;
    }


}

double Particles::calc_distance(glm::dvec3 p1, glm::dvec3 p2) {
    return sqrt(pow(p1.x - p2.x, 2) + pow(p1.y - p2.y, 2) + pow(p1.z - p2.z, 2));
}

double Particles::WPoly(double r, double h){
    if(0 < r && r <= h){
        return 315.0 / (64.0 * M_PI * pow(h, 9)) * pow((pow(h, 2) - pow(r, 2)), 3);
    }else{
        return 0;
    }
}

double Particles::WSpiky(double r, double h){
    if(0 < r && r <= h) {
        return -45.0 / (M_PI * pow(h, 6)) * pow((h - r), 2) / r;
    }else{
        return 0;
    }
}

double Particles::hash_position(glm::dvec3 pos)
{
    double length = h;
    int x = floor(pos.x/length);
    int y = floor(pos.y/length);
    int z = floor(pos.z/length);
    double rep = ((x + 31.0) * 37.0 + y) * 41.0 + z;
//    printf("%u\n", x);
//    printf("%u\n", y);
//    printf("%u\n", z);
    return rep;
}

void Particles::buildMap()
{
    for (const auto &entry : map) {
        delete(entry.second);
    }
    map.clear();

    for (Particle &par : particles){
        double num = hash_position(par.p);
        std::vector<Particle *> *ps;
        if(map.find(num) == map.end()) {
            ps = new std::vector<Particle *>();
        } else {
            ps = map[num];
        }
        ps->push_back(&par);
        map[num] = ps;
    }
}

void Particles::render(int tempx, int tempy)
{
    GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat mat_shininess[] = { 50.0 };
    GLfloat light_position[] = { 10.0, 10.0, 10.0, 0.0 };
    glShadeModel (GL_SMOOTH);
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);	
	bool color_change = true;
	bool dist_change = true;
	bool height_change = false;
	for (Particle &par : particles) {
		double distance = calc_distance(par.p, glm::dvec3(tempx, tempy, par.p.z));
		//printf("%f \n", distance);
		//printf("%d, %d\n", tempx, tempy);
		//printf("%f, %f\n", par.p.x, par.p.y);
		if (color_change) {
			glColorMaterial(GL_FRONT, GL_DIFFUSE);
			if (height_change) {
				if (par.p.y > 0.2) {
					glColor3f(1, 0.5, 0.8);
					color_change = false;
				}
				else if (par.p.y > 0) {
					glColor3f(1, 0.5, 0);
					color_change = false;
				}
				else if (par.p.y > -0.2) {
					glColor3f(0.8, 1, 0.5);
					color_change = false;
				}
				else if (par.p.y > -0.4) {
					glColor3f(0.5, 1, 0.8);
					color_change = false;
				}
				else if (par.p.y > -0.6) {
					glColor3f(0, 1, 0.5);
					color_change = false;
				}
				else if (par.p.y > -0.8) {
					glColor3f(0, 0.5, 1);
					color_change = false;
				}
			}
			if (dist_change) {
			if (distance <= 50) {
				glColor3f(1, 0.5, 0.8);
			}
			else if (distance <= 100) {
				glColor3f(1, 0.5, 0);
			}
			else if (distance <= 150) {
				glColor3f(0.8, 1, 0.5);
			}
			else if (distance <= 200) {
				glColor3f(0, 1, 0.5);
			}
			else if (distance <= 250) {
				glColor3f(1, 0.5, 0.8);
			}
			else if (distance <= 300) {
				glColor3f(1, 0.5, 0);
			}
			else if (distance <= 350) {
				glColor3f(0.8, 1, 0.5);
			}
			else if (distance <= 400) {
				glColor3f(0, 1, 0.5);
			}
			}
			else {
				glColor3f(0.2, 0.5, 0.8);
			}
			glColorMaterial(GL_FRONT, GL_SPECULAR);
			glColor3f(0.9, 0.9, 0.9);
			glColorMaterial(GL_FRONT, GL_AMBIENT);
			glColor3f(0.2, 0.5, 0.8);
		}
	}
    

	

    for(Particle &par : particles)
    {
        glPushMatrix();
        glTranslatef(par.p.x, par.p.y, par.p.z);
        glutSolidSphere(0.03, 10, 10);
        glPopMatrix();
        //printf("%f\n", par.p.x);
        //printf("%f\n", par.p.y);
        //printf("%f\n", par.p.z);
    }
    update();


    glPopAttrib();

}

bool Particles::collide(glm::dvec3 p) {
    if (p.y < 0.0)  return true;
    return false;
}
